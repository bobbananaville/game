import random
firstNameList=["Alex", "Bob", "Charlie", "Dan", "Evan", "Fargo", "Gillian", "Horace", "Ichigo", "Jack", "Klein"]
lastNameList=["SkullKicker", "Demigod", "The Unbreakable", "McChuckle", "Yellowbeak", "The Beast", "The Antagonist", "HeadHunter", "Apocalypto", "Greene"]
class fighter:
    def __init__(self, strength, defense, quickness, firstName, lastName, wins=0, bet=0):
        self.s=strength
        self.d=defense
        self.q=quickness
        self.fn=firstName
        self.ln=lastName
        self.w=wins
        self.b=bet
    def levelUp(self, statPoints):
        while(statPoints>0):
            chance=random.randint(1, 3)
            if(chance==1):
                self.s+=1
            elif(chance==2):
                self.d+=1
            else:
                self.q+=1
            if(self.s>10):
                self.s=10
            if(self.d>10):
                self.d=10
            if(self.q>10):
                self.q=10
            statPoints-=1

def charMakerA(fighterType):
    #return fighter(random.randint(2, 5), random.randint(1, 6), random.randint(3, 8))
    if(fighterType==1):
        return charMakerB(6, 8, 1, 1, 1, random.randint(6, 10))
    elif(fighterType==2):
        return charMakerB(2, 8, 1, 1, 1, random.randint(6, 10))
    elif(fighterType==3):
        return charMakerB(2, 4, 1, 1, 1, random.randint(6, 10))
    else:
        sChance=random.randint(1, 8)
        dChance=random.randint(sChance+1, 9)
        return charMakerB(sChance, dChance, 1, 1, 1, random.randint(6, 10))
def charMakerB(sChance, dChance, s, d, q, statPoints):
    if statPoints>0:
        chance=random.randint(1, 10)
        if(chance<=sChance and s<8):
            return charMakerB(sChance, dChance, s+1, d, q, statPoints-1)
        elif(chance<=dChance and d<8):
            return charMakerB(sChance, dChance, s, d+1, q, statPoints-1)
        elif(chance<=10 and q<8):
            return charMakerB(sChance, dChance, s, d, q+1, statPoints-1)
        else:
            return charMakerB(sChance, dChance, s, d, q, statPoints-1)
    else:
        return fighter(s, d, q, random.choice(firstNameList), random.choice(lastNameList))

def testFight(X, Y, XNo, YNo, Fatigue=0):
    print(X.fn+" Strength: "+str(X.s)+", "+X.fn+" Defense: "+str(X.d)+", "+X.fn+" Quickness: "+str(X.q))
    print(Y.fn+" Strength: "+str(Y.s)+", "+Y.fn+" Defense: "+str(Y.d)+", "+Y.fn+" Quickness: "+str(Y.q)+"\n")
    if(Fatigue<0):
        comboName=Y.fn
    else:
        comboName=X.fn
    if(Fatigue<-2 or Fatigue>2):
        print(comboName+" has dealt "+str(abs(int(Fatigue/2)))+" hits in a row now!\n")
    if(X.s==1):
        print(X.fn+" seems exceptionally weak right now.\n")
    if(Y.s==1):
        print(Y.fn+" seems exceptionally weak right now.\n")
    input("Press Enter to continue...\n")
    whoFirst=0
    while(whoFirst==0):
        whoFirst=random.randint(1, 10)-random.randint(1, 10)+X.q-Y.q-Fatigue
        if(whoFirst>0):
            Y=battle(X, Y)
            if(Fatigue<0):
                Fatigue=0
            Fatigue+=2
        elif(whoFirst<0):
            X=battle(Y, X)
            if(Fatigue>0):
                Fatigue=0
            Fatigue-=2
    
    if(X.s<=0):
        print(Y.fn + " " + Y.ln+" Wins!")
        Y.w+=1
        return [Y, YNo, XNo]
    elif(Y.s<=0):
        print(X.fn +" "+X.ln+" Wins!")
        X.w+=1
        return [X, XNo, YNo]
    else:
        return testFight(X, Y, XNo, YNo, Fatigue)
    
def battle(Offense, Defense):
    def calcDefense(result):
        if(result>5):
            return 1
        elif(result<-5):
            return 4
        elif(result>0):
            return 2
        else:
            return 3
    def attack(Offense, Defense):
        return random.randint(1, 10) - random.randint(1, 10) + Offense.s - Defense.d
    result = calcDefense(attack(Offense, Defense))
    if(result==1):
        print("\033[1;32;49m"+Offense.fn+ " strikes, dealing one damage to "+Defense.fn+".\033[0;37;49m")
        Defense.s-=1
    elif(result==2):
        print("\033[1;32;49m"+Offense.fn+ " Strikes, landing a critical hit and dealing two damage to "+Defense.fn+".\033[0;37;49m")
        Defense.s-=2
    elif(result==3):
        print("\033[1;32;49m"+Offense.fn+ " Strikes, smashing into "+Defense.fn+"'s shield and damaging it.\033[0;37;49m")
        if(Defense.d>0):
            Defense.d-=1
    else:
        print("\033[1;32;49m"+Offense.fn+ " Strikes, and misses entirely.\033[0;37;49m")
    print()
    return Defense

def stringGenerator(Offender, Defender, Result):
    stringChance=random.rand(1, 5)
    if(Result==1):
        if(stringChance==1):
            print(Offender.name+ " strikes, drawing "+Defender.name+"'s blood.")
        if(stringChance==2):
            print(Defender.name+" bites down a scream as "+Offender.name+" lands a blow.")
        #ADD MORE PER RESULT YOOOO


def calcRank(fighter):
    if(fighter.s>2):
        sOdd=fighter.s*2
    else:
        sOdd=fighter.s*1.4
    if(fighter.d>3):
        dOdd=fighter.d*1.6
    else:
        dOdd=fighter.d*1.2
    qOdd=fighter.q
    wOdd=fighter.w*2
    return [sOdd+dOdd+wOdd, fighter.q]

def calcOdds(X, Y):
    speedDiff=X[1]-Y[1]
    if(speedDiff>3):
        X[0]+=X[1]*(1+speedDiff*0.2)
        Y[0]+=Y[1]
    elif(speedDiff<-3):
        Y[0]+=Y[1]*(1-speedDiff*0.2)
        X[0]+=X[1]
    else:
        X[0]+=X[1]
        Y[0]+=Y[1]
    Z=X[0]+Y[0]
    XOdds=X[0]/Z
    YOdds=Y[0]/Z
    if XOdds>=0.2 and YOdds>=0.2:
        return [round(XOdds, 2)-0.1, round(YOdds, 2)-0.1]
    elif(XOdds<=0.2):
        return [0.15, 0.9]
    else:
        return [0.9, 0.15]

def bet(X, Y, value):
    odds=calcOdds(calcRank(X), calcRank(Y))
    fighterAWinProfit=int(round(odds[1]*100, 0)-10)
    fighterBWinProfit=int(round(odds[0]*100, 0)-10)
    print("On the left, "+X.fn+" "+X.ln+"! Betting on him will get you "+str(fighterAWinProfit)+ " buckeroos for every 10 bucks you spend!")
    print("On the right, "+Y.fn+" "+Y.ln+"! Betting on him will get you "+str(fighterBWinProfit)+ " buckeroos for every 10 bucks you spend!\n")
    bet=int(input("Press an odd number to bet on "+X.fn+" "+X.ln+
                  ", and an even integer to vote on "+Y.fn+" "+Y.ln+
                  ". Press 0 to not bet at all, and spend 5 Buckeroos on watching the show. "))
    betValue=0
    if(bet==0):
        betValue=0
        betProfit=0
    elif(bet%2==1):
        X.b=1
        Y.b=-1
        betProfit=fighterAWinProfit
    elif(bet%2==0):
        Y.b=1
        X.b=-1
        betProfit=fighterBWinProfit
    if(bet!=0):
        while(betValue*10>value or betValue<=0):
            betValue=int(input(("How many times do you want to bet?"+
                                "(Cost of each bet: 10 Buckeroos. You have: "+str(value)+" Buckeroos.")))
            if(betValue*10>value):
                print("You don't have enough Buckaroos to bet more than "+str(int(abs(round(value/10, 0)))) +" times.")
            elif(betValue<=0):
                print("It's too late to back out now; you need to bet at least once!")
    return [betValue, betProfit, X, Y]
            
def testGame():
    #testFight(charMakerA(random.randint(1, 3)), charMakerA(random.randint(1, 4)))
    fighterList=[charMakerA(random.randint(1, 3)),charMakerA(random.randint(1, 3)),charMakerA(random.randint(1, 3)),charMakerA(random.randint(1, 3)),charMakerA(random.randint(1, 4))]
    fin=False
    value=20
    daysLeft=12
    debt=2000
    while(fin==False):
        print("\033[0;37;49m\n")
        print("You have "+str(daysLeft)+" day(s) left to pay off your "+str(debt)+" buckaroo debt, and you have "+str(value) +" buckaroos to pay it with.")
        if(daysLeft==0):
            print("This is your very last chance. It's all or nothing!")
        #for fNo in range(0, 4):
        for fighter in fighterList:
            #fighter=fighterList[fNo]
            print (fighter.fn+" "+fighter.ln+" has had "+str(fighter.w)+" wins.\n")
        fighterA=random.randint(0,4)
        fighterB=-1
        while(fighterB==-1 or fighterB==fighterA):
            fighterB=random.randint(0,4)
        betStuff=bet(fighterList[fighterA], fighterList[fighterB], value)
        if(betStuff[0]!=0):
            value-=betStuff[0]*10
        else:
            value-=5
        resultArray=testFight(betStuff[2], betStuff[3], fighterA, fighterB)
        if resultArray[0].b==1:
            value+=betStuff[1]*betStuff[0]
            print("You won your bet! You now have "+str(value)+" Buckeroos!")
            resultArray[0].b=0
        elif resultArray[0].b==-1:
            print("You lost your bet! :( You now have "+str(value)+" Buckeroos!")
            resultArray[0].b=0
        if resultArray[0].w<3:
            resultArray[0].levelUp(random.randint(2, 4))
            fighterList[resultArray[1]]=resultArray[0]
        else:
            fighterList[resultArray[1]]=charMakerA(random.randint(1, 4))
        fighterList[resultArray[2]]=charMakerA(random.randint(1, 4))
        if(value<10):
            print("You've gone bankrupt! It looks like you'll have to pay it off in the fight pit. GAME OVER.")
            fin=True
        elif(value>=debt):
            print("You earned enough money to pay off your debt! WINNER!")
            fin=True
        elif(daysLeft==0):
            print("You've run out of time, and your debtor has gotten impatient! You've been forced to join the fight pit. GAME OVER")
        #fin=int(input("Please input 1 if you want to quit, or 0 if you want to continue."))
    input("Thank you for playing!")
testGame()