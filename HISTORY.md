#Version 0.01
    Created the BitBucket
    
#Version 0.02
    Betting system now active, not balanced
    Combat system now active
    Text is green when attacks occur
    
#Version 0.03
    Level up system now active - if a character wins, he gains XP to add to his attributes
    Characters die and are replaced if they lose.
    
#Versoin 0.04
    Quickness and Fatigue now fully implemented - rather than take turns, they roll quickness against one another.
    Days, Money and Debt are all now editable
    Names now prevented from repeating. Ungodly amount of names added to prevent the unlikely possibility of repeating names.
    
#Version 0.05
    Days, Money and Debt all now have default values.
    "X STRIKES IN A ROW" system has been deleted.