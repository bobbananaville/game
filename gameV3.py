import random
firstNameList=["Alex", "Bob", "Charlie", "Dan", "Evan", "Fargo", "Gillian", "Horace", "Ichigo", "Jack", "Klein", "Sam", "Taylor", "December", "Ender", "Leviathan", "David", "Jamie", "Varrick", "Elijah", "Emmet", "Ross", "Anton", "Nolan", "Sean", "Marco", "Mikel", "Zachary"]
lastNameList=["SkullKicker", "Demigod", "The Unbreakable", "McChuckle", "Yellowbeak", "The Beast", "The Antagonist", "HeadHunter", "Apocalypto", "Greene", "Hebert", "Anthrax", "The Goliath", "The Ungodly", "BloodRender", "LittleFoot", "SkySeer", "HeartStopper"]
safe=0  
class fighter:
    def __init__(self, strength, defense, quickness, firstName, lastName, wins=0, bet=0):
        self.s=strength
        self.d=defense
        self.q=quickness
        self.fn=firstName
        self.ln=lastName
        self.w=wins
        self.b=bet
    def levelUp(self, statPoints):
        if(self.s<3):
            self.s=2
        if(self.d<2):
            self.d=2
        while(statPoints>0):
            chance=random.randint(1, 3)
            if(chance==1):
                self.s+=1
            elif(chance==2):
                self.d+=1
            else:
                self.q+=1
            if(self.s>10):
                self.s=10
            if(self.d>10):
                self.d=10
            if(self.q>10):
                self.q=10
            statPoints-=1

def charMakerA(fighterType, name):
    #return fighter(random.randint(2, 5), random.randint(1, 6), random.randint(3, 8))
    if(fighterType==1):
        return charMakerB(6, 8, 1, 1, 1, random.randint(6, 10), name)
    elif(fighterType==2):
        return charMakerB(2, 8, 1, 1, 1, random.randint(6, 10), name)
    elif(fighterType==3):
        return charMakerB(2, 4, 1, 1, 1, random.randint(6, 10), name)
    else:
        sChance=random.randint(1, 8)
        dChance=random.randint(sChance+1, 9)
        return charMakerB(sChance, dChance, 1, 1, 1, random.randint(6, 10), name)
def charMakerB(sChance, dChance, s, d, q, statPoints, name):
    if statPoints>0:
        chance=random.randint(1, 10)
        if(chance<=sChance and s<8):
            return charMakerB(sChance, dChance, s+1, d, q, statPoints-1, name)
        elif(chance<=dChance and d<8):
            return charMakerB(sChance, dChance, s, d+1, q, statPoints-1, name)
        elif(chance<=10 and q<8):
            return charMakerB(sChance, dChance, s, d, q+1, statPoints-1, name)
        else:
            return charMakerB(sChance, dChance, s, d, q, statPoints-1, name)
    else:
        if(s==1):
            s=2  
        return fighter(s, d, q, name[0], name[1])
def displayStats(fighter):
    if(safe!=0):
        print(fighter.fn+" Strength: "+str(fighter.s)+", "+fighter.fn+" Defense: "+str(fighter.d)+", "+fighter.fn+" Quickness: "+str(fighter.q)+"\n")
    
def testFight(X, Y, XNo, YNo, FatigueX=0, FatigueY=0, combo=[0, 0]):
    displayStats(X)
    displayStats(Y)
    if(combo[0]==1):
        comboName=X.fn
    elif(combo[0]==2):
        comboName=Y.fn
    if(combo[1]>=2):
        print(comboName+" has dealt "+str(combo[1])+" hits in a row now!\n")
    if(X.s==1):
        print(X.fn+" seems exceptionally weak right now.\n")
    if(Y.s==1):
        print(Y.fn+" seems exceptionally weak right now.\n")
    input("Press Enter to continue...\n")
    whoFirst=0
    while(whoFirst==0):
        XRoll=random.randint(1, 10)
        YRoll=random.randint(1, 10)
        print(X.fn+" Rolled "+str(XRoll)+" + "+str(X.q)+" - "+str(FatigueX)+" (Fatigue) for Quickness.\n")
        print(Y.fn+" Rolled "+str(YRoll)+" + "+str(Y.q)+" - "+str(FatigueY)+" (Fatigue) for Quickness.\n")
        whoFirst=XRoll-YRoll+X.q-Y.q-FatigueX+FatigueY
        if(whoFirst>0):
            Y=battle(X, Y)
            FatigueX+=2
            if(combo[0]==1):
                combo[1]+=1
            else:
                combo[0]=1
                combo[1]=1
            FatigueY-=3
            if(FatigueY<0):
                FatigueY=0
        elif(whoFirst<0):
            X=battle(Y, X)
            FatigueY+=2
            if(combo[0]==2):
                combo[1]+=1
            else:
                combo[0]=2
                combo[1]=1
            FatigueX-=3
            if(FatigueX<0):
                FatigueX=0
    if(X.s<=0):
        print(Y.fn + " " + Y.ln+" Wins!")
        Y.w+=1
        return [Y, YNo, XNo]
    elif(Y.s<=0):
        print(X.fn +" "+X.ln+" Wins!")
        X.w+=1
        return [X, XNo, YNo]
    else:
        return testFight(X, Y, XNo, YNo, FatigueX, FatigueY, combo)
    
def battle(Offense, Defense):
    def calcDefense(result):
        print("Roll result: "+str(result))
        if(result>5):
            return 2
        elif(result<-5):
            return 4
        elif(result>0):
            return 1
        else:
            return 3
    def attack(Offense, Defense):
        OffenseRoll=random.randint(1, 10)
        DefenseRoll=random.randint(1, 10)
        print(Offense.fn+" rolled "+str(OffenseRoll+Offense.s)+" Strength against "+Defense.fn+ "'s Defense roll of "+str(Defense.d+DefenseRoll))
        return OffenseRoll - DefenseRoll + Offense.s - Defense.d
    result = calcDefense(attack(Offense, Defense))
    if(result==1):
        print("\033[1;32;49m"+Offense.fn+ " strikes, dealing one damage to "+Defense.fn+".\033[0;37;49m")
        Defense.s-=1
    elif(result==2):
        print("\033[1;32;49m"+Offense.fn+ " Strikes, landing a critical hit and dealing two damage to "+Defense.fn+".\033[0;37;49m")
        Defense.s-=2
    elif(result==3):
        print("\033[1;32;49m"+Offense.fn+ " Strikes, smashing into "+Defense.fn+"'s shield and damaging it.\033[0;37;49m")
        if(Defense.d>0):
            Defense.d-=1
    else:
        print("\033[1;32;49m"+Offense.fn+ " Strikes, and misses entirely.\033[0;37;49m")
    print()
    return Defense

def stringGenerator(Offender, Defender, Result):
    stringChance=random.randint(1, 5)
    if(Result==1):
        if(stringChance==1):
            print(Offender.name+ " strikes, drawing "+Defender.name+"'s blood.")
        if(stringChance==2):
            print(Defender.name+" bites down a scream as "+Offender.name+" lands a blow.")
        #ADD MORE PER RESULT YOOOO


def calcRank(fighter):
    if(fighter.s>2):
        sOdd=fighter.s*2
    else:
        sOdd=0
    if(fighter.d>3):
        dOdd=fighter.d*1.6
    else:
        dOdd=fighter.d*1.2
    qOdd=fighter.q
    wOdd=fighter.w*2
    return [sOdd+dOdd+wOdd, fighter.q]

def calcOdds(X, Y):
    speedDiff=X[1]-Y[1]
    if(speedDiff>3):
        X[0]+=X[1]*(1+speedDiff*0.1)
        Y[0]+=Y[1]
    elif(speedDiff<-3):
        Y[0]+=Y[1]*(1-speedDiff*0.1)
        X[0]+=X[1]
    else:
        X[0]+=X[1]
        Y[0]+=Y[1]
    Z=X[0]+Y[0]
    XOdds=X[0]/Z
    YOdds=Y[0]/Z
    if XOdds>=0.2 and YOdds>=0.2:
        return [round(XOdds, 2)-0.1, round(YOdds, 2)-0.1]
    elif(XOdds<=0.2):
        return [0.15, 0.9]
    else:
        return [0.9, 0.15]

def bet(X, Y, value):
    odds=calcOdds(calcRank(X), calcRank(Y))
    fighterAWinProfit=int(round(odds[1]*100, 0)-10)
    fighterBWinProfit=int(round(odds[0]*100, 0)-10)
    print("On the left, "+X.fn+" "+X.ln+"! Betting on him will get you "+str(fighterAWinProfit)+ " buckeroos for every 10 bucks you spend!")
    print("On the right, "+Y.fn+" "+Y.ln+"! Betting on him will get you "+str(fighterBWinProfit)+ " buckeroos for every 10 bucks you spend!\n")
    bet=int(input("Press an odd number to bet on "+X.fn+" "+X.ln+
                  ", and an even integer to vote on "+Y.fn+" "+Y.ln+
                  ". Press 0 to not bet at all, and simply watch the show."))
    betValue=0
    if(bet==0):
        betValue=0
        betProfit=0
    elif(bet%2==1):
        X.b=1
        Y.b=-1
        betProfit=fighterAWinProfit
    elif(bet%2==0):
        Y.b=1
        X.b=-1
        betProfit=fighterBWinProfit
    if(bet!=0):
        while(betValue*10>value or betValue<=0):
            betValue=int(input(("How many times do you want to bet?"+
                                "(Cost of each bet: 10 Buckeroos. You have: "+str(value)+" Buckeroos.")))
            if(betValue*10>value):
                print("You don't have enough Buckaroos to bet more than "+str(int(abs(round(value/10, 0)))) +" times.")
            elif(betValue<=0):
                print("It's too late to back out now; you need to bet at least once!")
    return [betValue, betProfit, X, Y]
  
def testGame():
    global safe
    #testFight(charMakerA(random.randint(1, 3)), charMakerA(random.randint(1, 4)))
    nameList=[]
    tempNameArray=[]
    while(len(nameList)<5):
        tempNameArray=[random.choice(firstNameList), random.choice(lastNameList)]
        if(len(nameList)==0):
            nameList.append(tempNameArray)
        elif(tempNameArray not in nameList):
            nameList.append(tempNameArray)
    fighterList=[charMakerA(random.randint(1, 3), nameList[0]),charMakerA(random.randint(1, 3), nameList[1]),charMakerA(random.randint(1, 3), nameList[2]),charMakerA(random.randint(1, 3), nameList[3]),charMakerA(random.randint(1, 4), nameList[4])]
    fin=False
    safe=int(input("Press 0 to turn off all stat displays. Press any other integer to turn them on"))
    value=20
    value=int(input("How much money do you wanna begin with? (Default: 20)"))
    daysLeft=12
    
    daysLeft=int(input("How many days do you wanna go? (Default: 12)"))
    debt=2000
    debt=int(input("How many buckeroos do you owe the mob? (Default: 2000)"))
    while(fin==False):
        print("\033[0;37;49m\n")
        print("You have "+str(daysLeft)+" day(s) left to pay off your "+str(debt)+" buckaroo debt, and you have "+str(value) +" buckaroos to pay it with.")
        if(daysLeft==0):
            print("This is your very last chance. It's all or nothing!")
        #for fNo in range(0, 4):
        for fighter in fighterList:
            #fighter=fighterList[fNo]
            print (fighter.fn+" "+fighter.ln+" has had "+str(fighter.w)+" wins.\n")
            displayStats(fighter)
        fighterA=random.randint(0,4)
        fighterB=-1
        while(fighterB==-1 or fighterB==fighterA):
            fighterB=random.randint(0,4)
        betStuff=bet(fighterList[fighterA], fighterList[fighterB], value)
        if(betStuff[0]!=0):
            value-=betStuff[0]*10
        resultArray=testFight(betStuff[2], betStuff[3], fighterA, fighterB)
        if resultArray[0].b==1:
            value+=betStuff[1]*betStuff[0]+betStuff[0]*10
            print("You won your bet! You now have "+str(value)+" Buckeroos!")
            resultArray[0].b=0
        elif resultArray[0].b==-1:
            print("You lost your bet! :( You now have "+str(value)+" Buckeroos!")
            resultArray[0].b=0
        if resultArray[0].w<4:
            resultArray[0].levelUp(random.randint(2, 4))
            fighterList[resultArray[1]]=resultArray[0]
        else:
            tempNameArray=[]
            while(tempNameArray==[] or tempNameArray in nameList):
                tempNameArray=tempNameArray=[random.choice(firstNameList), random.choice(lastNameList)]
            nameList.append(tempNameArray)
            fighterList[resultArray[1]]=charMakerA(random.randint(1, 4), tempNameArray)
        tempNameArray=[]
        while(tempNameArray==[] or tempNameArray in nameList):
            tempNameArray=tempNameArray=[random.choice(firstNameList), random.choice(lastNameList)]
        nameList.append(tempNameArray)
        fighterList[resultArray[2]]=charMakerA(random.randint(1, 4), tempNameArray)
        daysLeft-=1
        if(value<10):
            print("You've gone bankrupt! It looks like you'll have to pay it off in the fight pit. GAME OVER.")
            fin=True
        elif(value>=debt):
            print("You earned enough money to pay off your debt! WINNER!")
            fin=True
        elif(daysLeft<0):
            print("You've run out of time, and your debtor has gotten impatient! You've been forced to join the fight pit. GAME OVER")
            fin=True
        #fin=int(input("Please input 1 if you want to quit, or 0 if you want to continue."))
    input("Thank you for playing!")
    
testGame()